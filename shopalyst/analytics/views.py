
from django.http import Http404

from datetime import datetime

from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import generics

from analytics.models import (
  Action, Campaign, Publisher, Product, Merchant, Shopper, ParentOrg, ActionLog,
)
from analytics.serializers import (
  ActionSerializer, CampaignSerializer,
  PublisherSerializer, ProductSerializer,
  MerchantSerializer, ShopperSerializer,
  ParentOrgSerializer, ActionLogSerializer,
)

# Create your views here.

def queryset_filter_by_params(queryset, query_params):
  action = query_params.get('action')
  campaign_id = query_params.get('campaign_id')
  publisher_id = query_params.get('publisher_id')
  product_id = query_params.get('product_id')
  merchant = query_params.get('merchant')
  shopper_id = query_params.get('shopper_id')
  parent_org = query_params.get('parent_org')
  hashed_ip = query_params.get('hashed_ip')
  aff_source = query_params.get('aff_source')
  aff_medium = query_params.get('aff_medium')
  aff_term = query_params.get('aff_term')
  aff_campaign = query_params.get('aff_campaign')
  aff_content = query_params.get('aff_content')
  from_time_stamp = query_params.get('from_time_stamp', None)
  to_time_stamp = query_params.get('to_time_stamp', None)

  if action:
    queryset = queryset.filter(action__name=action)
  if campaign_id:
    queryset = queryset.filter(campaign_id__camp_id=campaign_id)
  if publisher_id:
    queryset = queryset.filter(publisher_id__name=publisher_id)
  if merchant:
    queryset = queryset.filter(merchant__name=merchant)
  if shopper_id:
    queryset = queryset.filter(shopper_id__name=shopper_id)
  if parent_org:
    queryset = queryset.filter(parent_org__name=parent_org)
  if hashed_ip:
    queryset = queryset.filter(hashed_ip=hashed_ip)
  if aff_source:
    queryset = queryset.filter(aff_source=aff_source)
  if aff_medium:
    queryset = queryset.filter(aff_medium=aff_medium)
  if aff_term:
    queryset = queryset.filter(aff_term=aff_term)
  if aff_campaign:
    queryset = queryset.filter(aff_campaign=aff_campaign)
  if aff_content:
    queryset = queryset.filter(aff_content=aff_content)
  if from_time_stamp:
    queryset = queryset.filter(
      time_stamp__gte=
      datetime.strptime(from_time_stamp, '%Y-%m-%dT%H:%M:%SZ')
    )
  if to_time_stamp:
    queryset = queryset.filter(
      time_stamp__lte=
      datetime.strptime(from_time_stamp, '%Y-%m-%dT%H:%M:%SZ')
    )

  return queryset

class ActionList(generics.ListCreateAPIView):
  queryset = Action.objects.all()
  serializer_class = ActionSerializer

class ActionDetail(generics.RetrieveUpdateDestroyAPIView):
  queryset = Action.objects.all()
  serializer_class = ActionSerializer

class ActionLogList(generics.ListCreateAPIView):
  # permission_classes = [permissions.IsAuthenticatedOrReadOnly]
  queryset = ActionLog.objects.all()
  serializer_class = ActionLogSerializer

  def get_queryset(self):
    queryset = queryset_filter_by_params(
      queryset=ActionLog.objects.all(),
      query_params=self.request.query_params,
    )

    return queryset

class ActionLogDetail(generics.RetrieveUpdateDestroyAPIView):
  # permission_classes = [permissions.IsAuthenticatedOrReadOnly]
  queryset = ActionLog.objects.all()
  serializer_class = ActionLogSerializer

@api_view()
def from_actionlogs_filter_unique(request):
  model_serializer_list = {
    'action':(Action, ActionSerializer),
    'campaign_id':(Campaign, CampaignSerializer),
    'publisher_id':(Publisher, PublisherSerializer),
    'product_id':(Product, ProductSerializer),
    'merchant':(Merchant, MerchantSerializer),
    'shopper_id':(Shopper, ShopperSerializer),
    'parent_org':(ParentOrg, ParentOrgSerializer),
  }

  unique_by = request.query_params.get('unique_by', None)

  if not unique_by or not model_serializer_list.get(unique_by):
    return Response(status=status.HTTP_400_BAD_REQUEST)

  queryset = queryset_filter_by_params(
    queryset=ActionLog.objects.all(),
    query_params=request.query_params,
  )

  Model,Serializer = model_serializer_list.get(unique_by)

  serializer = Serializer(
    Model.objects.filter(pk__in=queryset.values_list(unique_by, flat=True)),
    many=True,
  )
  return Response(serializer.data)

