from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from analytics import views

from django.views.decorators.cache import cache_page

urlpatterns = [
  path('actionlogs/', cache_page(60*60)(views.ActionLogList.as_view())),
  path('actionlogs/<int:pk>/', cache_page(60*60)(views.ActionLogDetail.as_view())),
  path('from_actionlogs_filter_unique/', cache_page(60*60)(views.from_actionlogs_filter_unique)),
]

urlpatterns = format_suffix_patterns(urlpatterns)
