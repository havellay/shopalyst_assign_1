from analytics import models

from datetime import datetime

def populate_shopper_actions_csv_in_db():
  fields = {
    'action': {
      'model': models.Action,
      'attrib': 'name',
    },
    'parent_org': {
      'model': models.ParentOrg,
      'attrib': 'name',
    },
    'campaign_id': {
      'model': models.Campaign,
      'attrib': 'camp_id',
    },
    'publisher_id': {
      'model': models.Publisher,
      'attrib': 'name',
    },
    'product_id': {
      'model': models.Product,
      'attrib': 'name',
    },
    'merchant': {
      'model': models.Merchant,
      'attrib': 'name',
    },
    'shopper_id': {
      'model': models.Shopper,
      'attrib': 'name',
    },

    'time_stamp': None,
    'hashed_ip': None,
    'user_agent': None,
    'aff_source': None,
    'aff_medium': None,
    'aff_term': None,
    'aff_campaign': None,
    'aff_content': None,
  }

  with open('/home/ubuntu/shopper_actions.csv') as f:
    lines = f.read()

  lines = lines.split('\n')

  keys = lines[0].split(',')

  lines_to_examine = []
  for l in lines[1:]:
    split_l = l.replace(', ', '__').split(',')
    if len(split_l) == len(keys):
      lines_to_examine.append(split_l)

  for line in lines_to_examine:
    action_log_entry = {
    }
    for i in range(len(line)):
      key = keys[i]
      value = line[i]
      model_details = fields.get(key)
      if model_details:
        model = model_details.get('model')
        attrib = model_details.get('attrib')
        entry = None
        try:
          entry = model.objects.get(**{attrib:value})
        except Exception as e:
          entry = model(**{attrib:value})
          entry.save()
        action_log_entry[key] = entry
      else:
        if key == 'time_stamp':
          value = datetime.strptime(value, '%Y-%m-%dT%H:%M:%SZ')
        action_log_entry[key] = value
    new_al_entry = models.ActionLog(**action_log_entry)
    new_al_entry.save()

  return


