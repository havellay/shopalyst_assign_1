from django.contrib import admin

from analytics import models
# Register your models here.

admin.site.register(models.Action)
admin.site.register(models.ParentOrg)
admin.site.register(models.Campaign)
admin.site.register(models.Publisher)
admin.site.register(models.Merchant)
admin.site.register(models.Shopper)
admin.site.register(models.Product)
admin.site.register(models.ActionLog)
