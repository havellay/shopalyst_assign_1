from django.db import models

# Create your models here.

class Action(models.Model):
  """
  'Action' table stores all known actions that are logged
  Attributes:
    - 'name' that can be up to 64 characters long;
      unnamed actions don't exist
  """
  # longest action name in shopper_actions.csv is 42 characters long;
  # no blank action names were found in shopper_actions.csv
  name = models.CharField(max_length=64, blank=False, unique=True)

  def __str__(self):
    return '{}'.format(self.name)

class Campaign(models.Model):
  """
  'Campaign' table stores all known campaigns
  Attributes:
    - 'name' that can be 128 characters long;
       unnamed campaigns can exist
  """
  # assuming that campaigns can have names and that the longest
  # campaign name is 128 characters long;
  camp_id = models.IntegerField(default=0, unique=True)

  def __str__(self):
    return '{}'.format(self.camp_id)

class Publisher(models.Model):
  """
  'Publisher' table
  Attributes:
    - 'name'
  During a GET request to ActionLog, publisher_id
  field should be Publisher.name;
  """
  # Maybe publisher shouldn't have a blank name, but certain
  # action log entries may not point to any publisher?
  name = models.CharField(max_length=128, blank=True, default='', unique=True)

  def __str__(self):
    return '{}'.format(self.name)

class Product(models.Model):
  """
  'Product' Table
  Attributes:
   - 'name'
  """
  # similar concerns as 'Publisher'
  name = models.CharField(max_length=128, blank=True, default='', unique=True)

  def __str__(self):
    return '{}'.format(self.name)

class Merchant(models.Model):
  """
  'Merchant' Table
  Attributes:
   - 'name'
  """
  name = models.CharField(max_length=128, blank=True, default='', unique=True)

  def __str__(self):
    return '{}'.format(self.name)

class Shopper(models.Model):
  """
  'Shopper' Table
  Attributes:
    - 'name'
  """
  name = models.CharField(max_length=128, blank=True, default='', unique=True)

  def __str__(self):
    return '{}'.format(self.name)

class ParentOrg(models.Model):
  """
  'ParentOrg' Table
  Attributes:
    - 'name'
  """
  name = models.CharField(max_length=128, blank=True, default='', unique=True)

  def __str__(self):
    return '{}'.format(self.name)

class ActionLog(models.Model):
  action = models.ForeignKey(Action, on_delete=models.PROTECT)
  campaign_id = models.ForeignKey(Campaign, blank=True, null=True, on_delete=models.PROTECT)
  publisher_id = models.ForeignKey(Publisher, blank=True, null=True, on_delete=models.PROTECT)
  product_id = models.ForeignKey(Product, blank=True, null=True, on_delete=models.PROTECT)
  merchant = models.ForeignKey(Merchant, blank=True, null=True, on_delete=models.PROTECT)
  shopper_id = models.ForeignKey(Shopper, blank=True, null=True, on_delete=models.PROTECT)
  parent_org = models.ForeignKey(ParentOrg, blank=True, null=True, on_delete=models.PROTECT)

  time_stamp = models.DateTimeField()
  hashed_ip = models.CharField(max_length=32, blank=True, default='')
  user_agent = models.CharField(max_length=512, blank=True, default='')
  aff_source = models.CharField(max_length=512, blank=True, default='')
  aff_medium = models.CharField(max_length=512, blank=True, default='')
  aff_term = models.CharField(max_length=512, blank=True, default='')
  aff_campaign = models.CharField(max_length=512, blank=True, default='')
  aff_content = models.CharField(max_length=512, blank=True, default='')

