from rest_framework import serializers
from analytics.models import (
  Action, Campaign, Publisher, Product, Merchant, Shopper, ParentOrg, ActionLog,
)

class ActionSerializer(serializers.ModelSerializer):
  class Meta:
    model = Action
    fields = ['id', 'name']

  def create(self, validated_data):
    return Action.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.name)

  def to_internal_value(self, data):
    obj,created = Action.objects.get_or_create(name=data)
    return obj

class CampaignSerializer(serializers.ModelSerializer):
  class Meta:
    model = Campaign
    fields = ['id', 'camp_id']

  def create(self, validated_data):
    return Campaign.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.camp_id = validated_data.get('camp_id', instance.camp_id)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.camp_id)

  def to_internal_value(self, data):
    obj,created = Campaign.objects.get_or_create(camp_id=data)
    return obj

class PublisherSerializer(serializers.ModelSerializer):
  class Meta:
    model = Publisher
    fields = ['id', 'name']

  def create(self, validated_data):
    return Publisher.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.name)

  def to_internal_value(self, data):
    obj,created = Publisher.objects.get_or_create(name=data)
    return obj

class ProductSerializer(serializers.ModelSerializer):
  class Meta:
    model = Product
    fields = ['id', 'name']

  def create(self, validated_data):
    return Product.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.name)

  def to_internal_value(self, data):
    obj,created = Product.objects.get_or_create(name=data)
    return obj

class MerchantSerializer(serializers.ModelSerializer):
  class Meta:
    model = Merchant
    fields = ['id', 'name']
  
  def create(self, validated_data):
    return Merchant.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.name)

  def to_internal_value(self, data):
    obj,created = Merchant.objects.get_or_create(name=data)
    return obj

class ShopperSerializer(serializers.ModelSerializer):
  class Meta:
    model = Shopper
    fields = ['id', 'name']

  def create(self, validated_data):
    return Shopper.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.name)

  def to_internal_value(self, data):
    obj,created = Shopper.objects.get_or_create(name=data)
    return obj

class ParentOrgSerializer(serializers.ModelSerializer):
  class Meta:
    model = ParentOrg
    fields = ['id', 'name']

  def create(self, validated_data):
    return ParentOrg.objects.create(**validated_data)

  def update(self, instance, validated_data):
    instance.name = validated_data.get('name', instance.name)
    instance.save()
    return instance

  def to_representation(self, value):
    return '{}'.format(value.name)

  def to_internal_value(self, data):
    obj,created = ParentOrg.objects.get_or_create(name=data)
    return obj

class ActionLogSerializer(serializers.ModelSerializer):
  action = ActionSerializer()
  campaign_id = CampaignSerializer()
  publisher_id = PublisherSerializer()
  product_id = ProductSerializer()
  merchant = MerchantSerializer()
  shopper_id = ShopperSerializer()
  parent_org = ParentOrgSerializer()

  class Meta:
    model = ActionLog
    # extra_kwargs = {
    #   'product_id':{
    #     'write_only':True,
    #   },
    # }
    fields = [
      'action', 'campaign_id', 'publisher_id', 'product_id', 'merchant',
      'shopper_id', 'parent_org', 'hashed_ip', 'user_agent', 'time_stamp',
      'aff_source', 'aff_medium', 'aff_term', 'aff_campaign', 'aff_content',
    ]

  def to_representation(self, instance):
    self.fields['action'] = ActionSerializer(read_only=True)
    self.fields['campaign_id'] = CampaignSerializer(read_only=True)
    self.fields['publisher_id'] = PublisherSerializer(read_only=True)
    self.fields['product_id'] = ProductSerializer(read_only=True)
    self.fields['merchant'] = MerchantSerializer(read_only=True)
    self.fields['shopper_id'] = ShopperSerializer(read_only=True)
    self.fields['parent_org'] = ParentOrgSerializer(read_only=True)
    return super(ActionLogSerializer, self).to_representation(instance)

  def create(self, validated_data):
    action_log = ActionLog(**validated_data)
    action_log.save()
    return action_log

  def update(self, instance, validated_data):
    instance.action = validated_data.get('action', instance.action)
    instance.campaign_id = validated_data.get('campaign_id', instance.campaign_id)
    instance.publisher_id = validated_data.get('publisher_id', instance.publisher_id)
    instance.product_id = validated_data.get('product_id', instance.product_id)
    instance.merchant = validated_data.get('merchant', instance.merchant)
    instance.shopper_id = validated_data.get('shopper_id', instance.shopper_id)
    instance.parent_org = validated_data.get('parent_org', instance.parent_org)
    instance.hashed_ip = validated_data.get('hashed_ip', instance.hashed_ip)
    instance.user_agent = validated_data.get('user_agent', instance.user_agent)
    instance.time_stamp = validated_data.get('time_stamp', instance.time_stamp)
    instance.aff_source = validated_data.get('aff_source', instance.aff_source)
    instance.aff_medium = validated_data.get('aff_medium', instance.aff_medium)
    instance.aff_term = validated_data.get('aff_term', instance.aff_term)
    instance.aff_campaign = validated_data.get('aff_campaign', instance.aff_campaign)
    instance.aff_content = validated_data.get('aff_content', instance.aff_content)

    instance.save()
    return instance
